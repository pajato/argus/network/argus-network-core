package com.pajato.argus.network.core

import kotlin.test.Test
import kotlin.test.assertEquals

class NetworkRepoErrorUnitTest {
    @Test fun `When creating a network repo error instance, verify behavior`() {
        val message = "some message"
        val networkRepoError = NetworkRepoError(message)
        assertEquals(message, networkRepoError.message)
    }
}
