package com.pajato.argus.network.core

import com.pajato.persister.jsonFormat
import kotlin.test.Test
import kotlin.test.assertEquals

class SelectableNetworkUnitTest {
    @Test fun `When serializing and deserializing, verify behavior`() {
        val (isSelected, isHidden, network) = Triple(false, false, Network(0))
        val selectableNetwork = SelectableNetwork(isSelected, isHidden, network)
        val json = jsonFormat.encodeToString(SelectableNetwork.serializer(), selectableNetwork)
        val deserialized = jsonFormat.decodeFromString(SelectableNetwork.serializer(), json)
        assertEquals(selectableNetwork, deserialized)
    }
}
