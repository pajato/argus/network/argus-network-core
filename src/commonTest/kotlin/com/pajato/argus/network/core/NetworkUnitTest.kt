package com.pajato.argus.network.core

import com.pajato.test.ReportingTestProfiler
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals

class NetworkUnitTest : ReportingTestProfiler() {
    @Test fun `When a default network is created, verify the values`() {
        val network = Network(id = 100, label = "Fred", iconUrl = "")
        assertEquals(100, network.id)
        assertEquals("Fred", network.label)
        assertEquals("", network.iconUrl)
    }

    @Test fun `When serializing a network object, verify the result`() {
        val data = Network(id = 1, label = "Argus", iconUrl = "argusAndroidIcon.jpeg")
        val json = """{"id":1,"label":"Argus","iconUrl":"argusAndroidIcon.jpeg"}"""
        assertEquals(json, Json.encodeToString(data))
        assertEquals(data, Json.decodeFromString(json))
    }
}
