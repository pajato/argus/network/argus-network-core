package com.pajato.argus.network.core

import com.pajato.i18n.strings.StringsResource

public object I18nStrings {
    public const val NETWORK_NOT_A_FILE: String = "NetworkNotAFile"
    public const val NETWORK_URI_ERROR: String = "NetworkUriError"

    public fun registerStrings() {
        StringsResource.put(NETWORK_NOT_A_FILE, "The given file with name '{{name}}' is not a valid file!")
        StringsResource.put(NETWORK_URI_ERROR, "No URI has been injected! A valid file URI is required.")
    }
}
