package com.pajato.argus.network.core

public typealias NetworkCache = MutableMap<Int, SelectableNetwork>
