package com.pajato.argus.network.core

import kotlinx.serialization.Serializable

@Serializable public data class Network(
    val id: Int,
    val packageId: String = "",
    val label: String = "",
    val shortLabel: String = "",
    val iconUrl: String = "",
)
