package com.pajato.argus.network.core

public class NetworkRepoError(message: String) : Exception(message)
