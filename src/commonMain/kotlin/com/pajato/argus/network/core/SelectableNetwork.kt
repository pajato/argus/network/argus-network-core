package com.pajato.argus.network.core

import com.pajato.argus.shared.core.ItemState
import kotlinx.serialization.Serializable

@Serializable public data class SelectableNetwork(
    override val isSelected: Boolean,
    override val isHidden: Boolean,
    val network: Network,
) : ItemState
