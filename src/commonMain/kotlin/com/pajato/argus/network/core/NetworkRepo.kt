package com.pajato.argus.network.core

import java.net.URI

public interface NetworkRepo {
    public val cache: NetworkCache
    public suspend fun injectDependency(uri: URI)
    public fun register(json: String)
    public fun register(item: SelectableNetwork)
    public fun toggleHidden(item: SelectableNetwork)
    public fun toggleSelected(item: SelectableNetwork)
}
